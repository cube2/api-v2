const express = require('express');
const colors = require('colors');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
var corsOptions = {
    origin: '*',
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
mongoose.connect(`mongodb+srv://${process.env.USER_BDD}:${process.env.MDP_BDD}@cluster0.ezrdh.mongodb.net/${process.env.NAME_BDD}?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex:true
    })
    .then(() => console.log(colors.blue('Connexion MongoDB success!')))
    .catch((e) => console.log(colors.red('Connexion MongoDB stranded !')));

app.use(bodyParser.json());
app.use(cors(corsOptions));


const homeRoutes = require('./routes/home');
const authRoutes = require('./routes/auth');
const groupRoutes = require('./routes/group');
const contentsRoute = require('./routes/contents');
const eventsRoute = require('./routes/events');

app.use('/', homeRoutes);
app.use('/api/auth', authRoutes);


app.use('/api',
    groupRoutes,
    contentsRoute,
    eventsRoute
);


module.exports = app;