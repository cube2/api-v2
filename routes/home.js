const express = require('express')
const app = express();


app.get('/',(req,res) =>{
    res.status(200).json({
        "api": true,
        "version": "V2",
        "message": "Welcome!"
    })
});

module.exports = app