const express = require('express')
const router = express.Router();
const groupCtrl = require('../controllers/group');

const authAccess = require('../middleware/auth')

router.post('/created_group',authAccess, groupCtrl.creatGroup);
router.get('/all_public_groups',authAccess, groupCtrl.allGroupPublic);
router.get('/all_groups',authAccess, groupCtrl.allGroup);
router.get('/get_group/:id',authAccess, groupCtrl.getGroup);
router.get('/user_groups',authAccess, groupCtrl.getUserGroup);
router.delete('/delete_group/:id',authAccess, groupCtrl.deleteGroup);
router.put('/leave_group/:id',authAccess, groupCtrl.leaveGroup);

module.exports = router;