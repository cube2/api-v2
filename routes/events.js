const express = require('express')
const router = express.Router();
const eventCtrl = require('../controllers/events');

const authAccess = require('../middleware/auth')

router.post('/create_event',authAccess, eventCtrl.sendEvent);

module.exports = router;