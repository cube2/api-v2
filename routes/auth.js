const express = require('express');
const router = express.Router();
const authCtrl = require('../controllers/auth');


router.post('/signup', authCtrl.signup);
router.post('/login', authCtrl.login);
router.get('/emailConfirmed/:id', authCtrl.emailConfirm);
router.post('/check_email', authCtrl.checkEmail);

module.exports = router;