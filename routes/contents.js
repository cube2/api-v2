const express = require('express')
const router = express.Router();
const groupCtrl = require('../controllers/contents');

const authAccess = require('../middleware/auth')

router.post('/send_content',authAccess, groupCtrl.sendContent);

module.exports = router;