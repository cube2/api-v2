const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


const userSchema = mongoose.Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    email: { type: String, required: true ,unique: true},
    password: { type: String, required: true },
    createAt: { type: Date, required: true },
    UpdateUser: { type: Date, required: false},
    picture: { type: String, required: false },
    phone: { type: Number, required: false },
    isActive: { type: Boolean, default: false },
    verifiedEmail: { type: Boolean, default: false },
    roles: {type: String, required:true},
});

userSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Users', userSchema);