const mongoose = require('mongoose');


const groupSchema = mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    picture: { type: String, required: true },
    isPublic: { type: Boolean, required: true },
    createAt: { type: Date, required: true },
    UpdateGroup: { type: Date, required: false},
    theme: { type: String, required: false },
    members: [{
        userId: {type: String, required: false},
        role: {type: String, required: false},
        isBanned: {type: Boolean, required: false, default : false},
        pendingInvitation: {type: Boolean, required: false, default: false},
        lastRead: {type: Date, required: false},
        createdAt: {type: Date, required: false},
    }],
    contents:  [{
        value: {type: String, required: false},
        author: {type: String, required: false},
        authorPicture: {type: String, required: false},
        createdAt: {type: Date, required: false},
        type: {type: String, required: false},
    }],
    events:  [{
        author: {type: String, required: false},
        name: {type: String, required: false},
        authorPicture: {type: String, required: false},
        dateEvent: {type: Date, required: false},
        createdAt: {type: Date, required: false},
        location: {type: String, required: false},
        picture: {type: String, required: false},
        contents:  [{
            value: {type: String, required: false},
            author: {type: String, required: false},
            authorPicture: {type: String, required: false},
            createdAt: {type: Date, required: false},
            type: {type: String, required: false},
        }],
        participants:  [{
            userId: {type: String, required: false},
            isPresent: {type: Boolean, required: false},
        }],
    }],
});

module.exports = mongoose.model('Groups', groupSchema);