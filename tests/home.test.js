const home = require('../routes/home')
const supertest = require('supertest')
const request = supertest

//test home page

describe(" un test home page ",() =>{
    test(" respond status 200", async ()=>{
        const res = await request(home).get("/")
        expect(res.statusCode).toBe(200)
    })
})