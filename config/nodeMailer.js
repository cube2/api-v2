var nodeMailer = require('nodemailer');


var mail = nodeMailer.createTransport({
    service: process.env.STMP_SERVICE,
    auth: {
        user: process.env.STMP_USER,
        pass: process.env.STMP_MDP
    }
});

module.exports = mail