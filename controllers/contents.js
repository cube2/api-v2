const Group = require('../models/groups');
const User = require('../models/users');

exports.sendContent = (req, res, next)=>{
    req.app.io.emit('news', {key:"value"});
    // io.emit('news','Voici un nouvel élément envoyé par le serveur')
    let idGroup = req.body.id_group;
    User.findOne({_id: req.userId}).then(user =>{
        Group.findOne({_id : idGroup}).then(group =>{
            let newContent = {
                value: req.body.value,
                author: user._id,
                authorPicture: user.picture,
                createdAt: new Date(),
                type: req.body.type,
            };
            group.contents.push(newContent);
            group.save();
            res.status(200).json({message:'Content created'})
        }).catch(e => res.status(400).json({e}))
    }).catch(e => res.status(400).json({e}))
}