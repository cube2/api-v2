const User = require('../models/users');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.signup = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            delete req.body._id;
            const user = new User({
                ...req.body,
                createAt: new Date(),
                roles: 'Contributor',
                password: hash
            });
            user.save()
                .then(() => res.status(201).json({ message: 'User created !'}))
                .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));

};

exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(401).json({ error: 'User not found !' });
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ error: 'password is wrong!' });
                    }
                    res.status(200).json({
                        id: user._id,
                        token: jwt.sign(
                            {userId: user._id},
                            process.env.JWT_SECRET_KEY,
                            {expiresIn: process.env.JWT_TIME_EXPERED}
                        ),
                        firstname: user.firstname,
                        lastname: user.lastname,
                        picture: user.picture,
                        role: user.roles,
                    });
                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};

exports.emailConfirm = (req, res, next) => {
    User.findOne({_id: req.params.id}).then(user =>{
        if (!user) {
            return res.status(401).json({ error: 'User not found !' });
        }
        user.verifiedEmail = true;
        user.isActive = true;
        user.save().then(() => res.status(202).json({
            'message':'email confirmed',
            'user': user
        }));
    }).catch(e => res.status(500).json({e}))


};


exports.checkEmail = (req, res, next) =>{
    User.findOne({email:req.body.email}).then(user =>{
        if (!user) {
            return res.status(202).json({ error: 'User not found !' });
        }
        return res.status(404).json({ error: 'User exist !' });
    }).catch(e =>{
        res.status(500).json({e})
    })
}