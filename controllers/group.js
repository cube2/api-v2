const Group = require('../models/groups');

exports.creatGroup = (req, res, next)=>{
        const role = 'Contributor'
        delete req.body._id;
        const group = new Group({
            ...req.body,
            createAt: new Date(),
            members: {
                userId : req.userId,
                role : role,
                isBanned : false,
                pendingInvitation : false,
                lastRead : new Date(),
            }
        });
        group.save()
            .then(() => res.status(201).json({ message: 'Group created !'}))
            .catch(error => res.status(400).json({ error }));
}

exports.allGroupPublic = (req, res, next)=>{
    Group.find({isPublic : true}).then(group =>{
        res.status(200).json(group)
    }).catch(e => res.status(400).json({e}))
}
exports.allGroup = (req, res, next)=>{
    Group.find({}).then(group =>{
        res.status(200).json(group)
    }).catch(e => res.status(400).json({e}))
}

exports.deleteGroup = (req, res, next)=>{
    Group.deleteOne({_id: req.params.id}).then(group =>{
            res.status(200).json({message : 'Group delete'})
    }).catch(e => res.status(400).json({e}))
}
exports.getUserGroup = (req, res, next)=>{
    Group.find({ "members.userId": req.userId}).sort({createAt: 'desc'}).then(group =>{
        res.status(200).json(group)
    }).catch(e => res.status(400).json({e}))
}
exports.getGroup = (req, res, next)=>{
    Group.findOne({_id: req.params.id}).then(group =>{
            res.status(200).json(group)
    }).catch(e => res.status(400).json({e}))
}
exports.leaveGroup = (req, res, next)=>{
    Group.findOne({_id: req.params.id}).findOne({ "members.userId": req.userId}).then(group =>{
        group.members.isBanned= true;
        group.save();
        res.status(200).json('Member is banned');
    }).catch(e => res.status(400).json({e}))
}