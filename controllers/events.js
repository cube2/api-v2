const Group = require('../models/groups');
const User = require('../models/users');

exports.sendEvent = (req, res, next)=>{
    User.findOne({_id: req.userId}).then(user =>{
        Group.findOne({_id: req.body.id_group}).then(group =>{
            let event = {
                author: user._id,
                name: req.body.name,
                authorPicture: user.picture,
                dateEvent: req.body.dateEvent,
                createdAt: new Date(),
                location: req.body.location,
                picture: req.body.picture,
                participants: {
                    userId: user._id,
                    isPresent: true
                }
            };
            group.events.push(event);
            group.save();
            res.status(200).json({message:'Event created'})
        }).catch(e => res.status(400).json({e}))
    }).catch(e => res.status(400).json({e}))
}